from django.db import models
from Locations.models import Location
from Employees.models import Employee

class Media(models.Model):
    FORMAT_CHOICES = (
        ('book', 'Book'),
        ('magazine', 'Magazine'),
        ('newspaper', 'Newspaper'),
        ('cd', 'CD'),
        ('dvd', 'DVD'),
        ('other', 'Other'),
    )

    STATUS_CHOICES = (
        ('out', 'Checked Out'),
        ('in', 'Checked In'),
        ('repair', 'In Repair'),
        ('withdrawn', 'Withdrawn'),
    )

    GENRE_CHOICES = sorted(
    (
        ('historical_fiction', 'Historical Fiction'),
        ('science_fiction', 'Science Fiction'),
        ('mystery', 'Mystery'),
        ('horror', 'Horror'),
        ('romance', 'Romance'),
        ('graphic_novel', 'Graphic Novel'),
        ('young_adult', 'Young Adult'),
        ('fiction', 'Fiction'),
        ('narrative', 'Narrative'),
        ('nonfiction', 'Nonfiction'),
        ('historical_nonfiction', 'Historical Nonfiction'),
        ('travel', 'Travel'),
        ('crime', 'Crime'),
        ('humor', 'Humor'),
        ('childrens', "Children's"),
        ('spirituality', 'Spirituality'),
        ('memoir', 'Memoir'),
        ('fantasy', 'Fantasy'),
        ('autobiography', 'Autobiography'),
        ('poetry', 'Poetry'),
    ),
    key=lambda x: x[1]
    )

    format = models.CharField(max_length=20, choices=FORMAT_CHOICES)
    author = models.CharField(max_length=100)
    title = models.CharField(max_length=1000)
    publisher = models.CharField(max_length=100)
    genre = models.CharField(max_length=50, choices=GENRE_CHOICES)
    description = models.TextField(blank=True, null=True)
    date_added = models.DateField(auto_now_add=True)
    date_published = models.DateField()
    call_number = models.CharField(max_length=50)
    ISBN = models.CharField(max_length=20)
    cover_art = models.URLField()
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    location = models.ForeignKey(Location, on_delete=models.SET_DEFAULT, default='Unknown')
    entered_by = models.ForeignKey(Employee, on_delete=models.SET_DEFAULT, default='Unknown')

    def __str__(self):
        return f"{self.format} - {self.author} - {self.title}"
