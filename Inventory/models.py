from django.db import models
from Locations.models import Location
from Patrons.models import Patron
from Media.models import Media

class Inventory(models.Model):
    name = models.ForeignKey(Media, related_name="mediaName", on_delete=models.CASCADE)
    location = models.ForeignKey(Location, related_name="mediaLocation", on_delete=models.CASCADE)
    status = models.ForeignKey(Media,related_name="mediaStatus", on_delete=models.CASCADE)
    borrower = models.ForeignKey(Patron, related_name="mediaPatron", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} - {self.location}"
