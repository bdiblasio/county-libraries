from django.db import models
from Locations.models import Location

class Employee(models.Model):
    name = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    department = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=200)
    hire_date = models.DateField()
    location = models.ForeignKey(Location, on_delete=models.SET_DEFAULT, default='Unknown')

    def __str__(self):
        return self.name
